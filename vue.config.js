let publicPath = '/app/';

if (process.env.PUBLIC_PATH) {
  publicPath = process.env.PUBLIC_PATH;
} else if (process.env.NODE_ENV === 'production') {
  publicPath = '/app/';
}

module.exports = {
  publicPath,
  chainWebpack: (config) => {
    config.module
      .rule('i18n')
      .resourceQuery(/blockType=i18n/)
      .type('javascript/auto')
      .use('i18n')
      .loader('@kazupon/vue-i18n-loader')
      .end();
  },
};
