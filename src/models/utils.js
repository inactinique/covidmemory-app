export const getValue = (item, key, defaultValue) => {
  if (!item[key]) {
    return defaultValue;
  }
  if (!Array.isArray(item[key])) {
    return item[key]['@value'] || defaultValue;
  }
  if (!item[key].length) {
    return defaultValue;
  }
  return item[key][0]['@value'];
};

export const COLORS = [
  '456990',
  '028090',
  '114B5F',
  '456990',
  '985277',
  '5C374C',
  'FAA275',
  '985277',
  '5C374C',
];
