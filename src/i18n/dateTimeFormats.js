export default {
  en: {
    year: {
      year: 'numeric',
    },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
      hour: 'numeric',
      minute: 'numeric',
    },
    short: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
    },
  },
  de: {
    year: {
      year: 'numeric',
    },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
      hour: 'numeric',
      minute: 'numeric',
    },
    short: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
    },
  },
  fr: {
    year: {
      year: 'numeric',
    },
    long: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
      hour: 'numeric',
      minute: 'numeric',
    },
    short: {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      weekday: 'short',
    },
  },
};
