export default {
  en: {
    actions: {
      accept: 'accept',
      submit: 'submit',
      createMemory: 'Add my memory!',
    },
    createdBy: 'by <b>{creator}</b>',
    usefulLinks: 'quick links:',
    project: {
      title: '#covidmemory',
      more: '(read more about this project)',
    },
    views: {
      create: {
        title: 'Add my memory to the collection',
        subheading: 'complete the form to send your picture or videos. Fields marked with *',
      },
      intro: {
        title: 'COVID-19 <span class="h">memories</span>',
        subheading: `A platform to collect COVID19 related photos, videos, stories and interviews
        from/with ordinary people living or working in Luxembourg`,
      },
      terms: {
        title: 'Terms of use',
        subheading: `Consultation of this site is
        subject to the terms and conditions set out below.`,
      },
      about: {
        title: 'About',
        subheading: `<b>covidmemory.lu</b> is a free and open online platform to
        collect memories of the COVID19 crisis in Luxembourg`,
      },
      notFound: {
        title: 'Not found',
        subheading: '',
      },
      privacy: {
        title: '',
        subheading: '',
      },
    },
    to: {
      index: 'index',
      memories: 'memories',
      about: 'about',
      intro: 'introduction',
      create: 'add yours',
      terms: 'terms of use',
      privacy: 'Privacy',
    },
    languages: 'languages',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
    },
    welcome: 'Welcome!',
  },
  fr: {
    actions: {
      accept: 'accept',
      submit: 'submit',
      createMemory: 'Ajouter ma contribution',
    },
    createdBy: 'by <b>{creator}</b>',
    usefulLinks: 'quick links:',
    project: {
      title: '#covidmemory',
      more: '(read more about this project)',
    },
    views: {
      create: {
        title: 'Add my memory to the collection',
        subheading: 'complete the form to send',
      },
      intro: {
        title: 'COVID-19 memories',
        subheading: `La plateforme pour collecter photos, vidéos, récits et
        témoignages, liés au COVID-19, de personnes habitant ou
        travaillant au Luxembourg.`,
      },
      terms: {
        title: 'Conditions d\'utilisation',
      },
      about: {
        title: 'À propos',
        subheading: `<b>covidmemory.lu</b> est une plateforme en ligne gratuite et
        ouverte pour collecter des souvenirs de la crise du Covid19 au Luxembourg.`,
      },
      notFound: {
        title: 'Not found',
        subheading: '',
      },
      privacy: {
        title: '',
        subheading: '',
      },
    },
    to: {
      index: 'accueil',
      memories: 'souvenirs',
      about: 'about',
      intro: 'introduction',
      create: 'add yours!',
      terms: 'terms of use',
      privacy: 'Privacy',
    },
    languages: 'langues',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
    },
    welcome: 'Willkommen!',
  },
  de: {
    actions: {
      accept: 'accept',
      submit: 'submit',
      createMemory: 'Beitrag einreichen',
    },
    createdBy: 'by <b>{creator}</b>',
    usefulLinks: 'quick links:',
    project: {
      title: '#covidmemory',
      more: '(mehr über das Projekt)',
    },
    views: {
      create: {
        title: 'Add my memory to the collection',
        subheading: 'complete the form to send',
      },
      intro: {
        title: 'COVID-19 memories',
        subheading: `Plattform zur Sammlung von Fotos,
        Videos und Geschichten zur COVID-19 Krise in Luxemburg.`,
      },
      terms: {
        title: 'Nutzungsbedingungen',
      },
      about: {
        title: 'Über uns',
        subheading: `<b>covidmemory.lu</b> ist eine freie und offene Online-Plattform,
        die Erinnerungen an die COVID19 Krise in Luxemburg sammelt.`,
      },
      notFound: {
        title: 'Not found',
        subheading: '',
      },
      privacy: {
        title: '',
        subheading: '',
      },
    },
    to: {
      index: 'index',
      memories: 'Erinnerungen',
      about: 'about',
      intro: 'introduction',
      create: 'add yours!',
      terms: 'terms of use',
      privacy: 'Privacy',
    },
    languages: 'Sprachen',
    lang: {
      en: 'English',
      fr: 'Français',
      de: 'Deutsch',
    },
    welcome: 'Willkommen!',
  },
};
